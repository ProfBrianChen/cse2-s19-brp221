//hw10partB
//Bratislav Petkovic 
//Step 1: Write a method to generate a shuffled deck of cards represented as an array of 52 integers.
//Step 2: Write a method that draws the first 5 card hand from a shuffled deck, represented as the first 5 cards in the array.
//Step 3: Write a method that accepts a 5 card hand and an integer k as input.  It should search
//for the kth lowest card.  So if k is 1, it searches for the lowest card.  If k is 2,
//it searches for the second lowest card, etc. If k is greater than 5 or less than zero, it returns an error.  Use linear search.
//Finally, write a method that calls the method in step 3 to search for your straight.
//cards are to be represented using the numbers 0-51, where 0-12 are the diamonds, 
//13-25 are the clubs, 26-38 are the hearts, and the rest are the spades
import java.util.Random;
import java.util.Arrays;

public class Straight{
  
  public static int[] shuffledDeck(){
    //creating the deck in a sorted increasing order
    int[] deckArray = new int[52] ;
    for (int a =0; a <deckArray.length; a++){
      deckArray[a] = a ;
    }
    
      Random rand = new Random(); 
         //shuffling 52 times 
        for (int i = 0; i < 52; i++) { 
            // Random for remaining positions. 
            int r =   rand.nextInt(52 );               
             //swapping the elements 
             int temp = deckArray[r]; 
             deckArray[r] = deckArray[i]; 
             deckArray[i] = temp; 
        }
    return deckArray;
  }
  
  public static void printArray(int[] array){
    System.out.print("{");
    for(int a=0;a<array.length;a++){
         System.out.print(" " + array[a] );
    }
    System.out.print("}");
  }
  
  public static int[] firstFive(int[] cardsArray){
    int[] firstFiveCards = new int[5];
    for(int i = 0;i<firstFiveCards.length;i++){
      firstFiveCards[i] = cardsArray[i] ;
    }
    return firstFiveCards ;
  }
  
  public static int[] linearSearch(int[] handArray){
    Arrays.sort(handArray);
    //printArray(handArray);
    //System.out.println("");
    
    //taking into account the differetn suits 
    for(int i = 0 ; i<handArray.length;i++){
      //diamonds
      if(handArray[i]<=12){
        handArray[i] = handArray[i]  ; 
      }
      
       //clubs
      else if(handArray[i]<=25 && handArray[i]>=13){
        handArray[i] = (handArray[i] % 13)  ;
      }
      
       //diamonds
     else if(handArray[i]<= 38 && handArray[i]>=26){
        handArray[i] = (handArray[i] % 26)  ;
     }
      
       //diamonds
      if(handArray[i]<=51){
         handArray[i] = (handArray[i] % 39) ;
      }
      
    }
    
    Arrays.sort(handArray);
    return handArray ;

  }
  
  public static boolean IsAStraight(int[] sortedHandArray){
    boolean IsAStraight = true;
    for(int i = 0; i< 4; i++){
      if (sortedHandArray[i] != ( sortedHandArray[i+1] - 1)){
        IsAStraight = false; 
      }
      else{ continue;}
    }
    return IsAStraight ; 
  }
  
   
  public static void main(String[] args){
   //System.out.println("");
   boolean IsAStraight ;
    double count = 0 ; 
    for(int p = 0; p<1000000; p++){
      //System.out.println("");
      int[] cardsArray = shuffledDeck(); 
      //printArray(cardsArray) ;
      //System.out.println("");
      int[] potentialStraight = firstFive(cardsArray);
      ///printArray(potentialStraight);
      //System.out.println("");
      //printArray(linearSearch(potentialStraight));
      potentialStraight = linearSearch(potentialStraight);
      //System.out.println("");
      IsAStraight = IsAStraight(potentialStraight) ;
      //System.out.println(IsAStraight);
      if(IsAStraight){
        //printArray(potentialStraight);
        count ++ ;
      }
      
      
    }
    System.out.println(count);
    double probability = count/10000;
    System.out.println("The probability of drawing a straight is " +probability + "%");
   
  
  }
}
