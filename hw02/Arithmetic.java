//hw 03 

import java.util.Scanner;

public class Arithmetic {
    	// main method required for every Java program
   	public static void main(String[] args) {
          //Number of pairs of pants
          int numPants = 3;
          
          //Cost per pair of pants
          double pantsPrice = 34.98;

          //Number of sweatshirts
          int numShirts = 2;
          
          //Cost per shirt
          double shirtPrice = 24.99;

          //Number of belts
          int numBelts = 1;
          
          //cost per belt
          double beltCost = 33.99;

          //the tax rate
          double paSalesTax = 0.06;
          
          //total cost of pants
      	  double totalCostOfPants;  
          totalCostOfPants = numPants * pantsPrice;
      
          //total cosy of shirts
          double totalCostOfBelts;
          totalCostOfBelts = numBelts * beltCost;
            
          //total cost of shirts
          double totalCostOfShirts;
          totalCostOfShirts = numShirts * shirtPrice;
            
          //sales tax for pants
          double salesTaxPants;
          salesTaxPants = totalCostOfPants * paSalesTax;
      
          //sales tax for belts
          double salesTaxBelts;
          salesTaxBelts = totalCostOfBelts * paSalesTax;
      
          //sales tax for shirts
          double salesTaxShirts;
          salesTaxShirts = totalCostOfShirts * paSalesTax;
      
          //total cost before tax
          double totCostBeforeTax;
          totCostBeforeTax = totalCostOfShirts + totalCostOfBelts + totalCostOfPants;
          System.out.print("the total cost of your purchase before tax is $") ;
          System.out.printf("%.2f", totCostBeforeTax);
          System.out.println("");
            
          //total sales tax
          double totSalesTax;
          totSalesTax = salesTaxShirts + salesTaxBelts + salesTaxPants;
          System.out.print("the total cost of your sales tax is $");
          System.out.printf("%.2f",totSalesTax);
          System.out.println("");
      
      
          //the total that the customer paid
          double totalPaid;
          totalPaid = totCostBeforeTax + totSalesTax;
          System.out.print("the total cost of your purchase is $"); 
          System.out.printf("%.2f",totalPaid);
          System.out.println("");


	}  //end of main method   
} //end of class
