//The program asks the user for the type, name, professors name, meetup times, start time
//and codenumber of his/her classes.
 

  import java.util.Scanner;   
  
  public class Hw05{      
  public static void main(String[] args){   
 
  Scanner userInput = new Scanner(System.in);  //myScanner class
  double bin = 2.0;  //declaring bin 
   
   //Course Number 
   System.out.println("Enter the course number : "); 
   while(!userInput.hasNextInt()){      
        System.out.println("Error: Enter the course number:  ");    
        userInput.next();                                      
    }
     int courseNumber = userInput.nextInt();  
    
    //Number of Times a week the class meets
    System.out.println("How many times does the class meet in a week:  ");   
    while(!userInput.hasNextInt()){  
        System.out.println("Error: Enter an integer:   ");  
        userInput.next();
    }
     int meetupsPerWeek = userInput.nextInt();  
    
    //Number of students in a class
    System.out.println("How many students are in the class:   "); 
      while(!userInput.hasNextInt()){  
        System.out.println("Error: Enter an integer:  "); 
        userInput.next();
     }
     int numbStudents = userInput.nextInt(); 
    
    //Department Name
    System.out.println("Enter the department name:   "); 
      while (userInput.hasNextDouble()==true) {  
        bin = userInput.nextDouble();
        System.out.println("Error:Try again:   "); 
    }
        String deptName = userInput.next();   
    
    //Professors name
    System.out.println("Enter the professor's name:   ");  
      while (userInput.hasNextDouble()==true) {  
        bin = userInput.nextDouble();
        System.out.println("Error: Try again:   "); 
    } 
        String professorName = userInput.next(); 
    
    //Class start time
    System.out.println("Enter the class start time:    ");  
      while (userInput.hasNextDouble()==true) { 
        bin = userInput.nextDouble();
        System.out.println("Error: Try again:     "); 
    }
       String startTime = userInput.next();  
   
    //Printing the course information
    System.out.println("") ;
    System.out.println("Course number : " + courseNumber + ".");  
    System.out.println("Class meets : " + meetupsPerWeek + " times per week.");
    System.out.println("Number of students : " + numbStudents + ".");
    System.out.println("Department name : " + deptName + ".");
    System.out.println("Professor's name : " + professorName + ".");
    System.out.println("Class begins at : " + startTime + ".");
    

    
  }  

}  