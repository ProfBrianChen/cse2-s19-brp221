//Bratislav Petkovic 
//April 5th 2019
//This program takes a user string and alphabetically sorts it by two methods. 
import java.util.Arrays ;
import java.lang.Character ;

import java.util.ArrayList;


public class Program1{
  
 
 public static char[] getAtoM(char[] array){
   Arrays.sort(array);
   System.out.println("");
   System.out.print("AtoM characters: ");
   //first we go through the whole list and count how many characters are a to m 
   int count1 = 0;
   for (int b=0;b<array.length;b++){
     if(array[b] >= 'A'& array[b] <= 'M' || array[b] >= 'a'& array[b] <= 'm'){
       count1 ++;
     }
   }
   
   //then we make a new array
   char atomArray [];
   atomArray = new char[count1];
   for (int c=0;c<array.length;c++){
     // we only look for characters that are between a and m
     if(array[c] >= 'A'& array[c] <= 'M' || array[c] >= 'a'& array[c] <= 'm'){
       //this is a for loop for indexing. I use break because I want to exit 
       //the loop after every index. I need the index to go 0,1,2,3,4...
       for (int d=0;d<count1 -1;d++){       
          atomArray[d]= array[c] ;
          System.out.print(atomArray[d]);
          break;
       }
      }
   }
   
   
   System.out.println("");
   return atomArray ;
 }
  
 public static char[] getNtoZ(char[] array){
   Arrays.sort(array);
   System.out.print("NtoZ characters: ");
   //first we go through the whole list and count how many characters are a to m 
   int count1 = 0;
   for (int b=0;b<array.length;b++){
     if(array[b] >= 'N'& array[b] <= 'Z' || array[b] >= 'n'& array[b] <= 'z'){
       count1 ++;
     }
   }
   
   //then we make a new array
   char ntozArray [];
   ntozArray = new char[count1];
   for (int c=0;c<array.length;c++){
     // we only look for characters that are between n and z
     if(array[c] >= 'N'& array[c] <= 'Z' || array[c] >= 'n'& array[c] <= 'z'){
       //this is a for loop for indexing. I use break because I want to exit 
       //the loop after every index. I need the index to go 0,1,2,3,4...
       for (int d=0;d<count1 -1;d++){       
          ntozArray[d]= array[c] ;
          System.out.print(ntozArray[d]);
          break;
       }
      }
   }
   
   
   return ntozArray ;
   }
 
  
  public static void main(String[] args){
    System.out.println("");
    //array of random size 
    int stringLength = ((int)(Math.random()*25)+5);
       
    //seconnd we create empty char array
    char charArray [] ;
    charArray = new char[stringLength] ;
    System.out.print("Random character array: ");
    
    //third we create an array of characters
    for (int a = 0; a<stringLength;a++){
      //doubling randChar 
      double randChar = Math.random()*2; 
			if(randChar>1){ 
				charArray[a]=(char)((int)(Math.random()*25)+98);
			}
			else if(randChar<=1){ 
				charArray[a]=(char)((int)(Math.random()*25)+66);
    }System.out.print(charArray[a] + ", ");
    }
    
    getAtoM(charArray);
    getNtoZ(charArray);
    System.out.println("");
    System.out.println("");
    
  }
  
}

