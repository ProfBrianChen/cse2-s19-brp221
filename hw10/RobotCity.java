/*
write a method called buildCity() that randomly generates the east-west and north-south
dimensions (in blocks) of the city as values from 10 to 15.  Have buildCity() assign a random 
integer value between 100 and 999 to each city block to represent the population of the block.

display() that accepts the city array as input.  It prints the city’s block level populations out using the printf() format string. 

invade() that accepts the city array and a random integer k as input.  k will be the number of 
invading robots.  For each robot, randomly generate block coordinates in the city where the robot
lands, ensuring that no robot lands on top of another robot. Set the integer value of the landing block
to the negative value of its original value to denote the occupancy of the robot.

update() that accepts the city array as input.  update() moves the robots from
their current block to the next block eastwards by manipulating the negative values in the 
array.  The robots at the eastern-most limits of the city disappear.  New robots do not appear at western city limits.  

In your main method, build the city, display it, invade the city, display it, then update and display in a loop 5 times.
*/

//Bratislav Petkovic 
//April28th,2019
//HW10

import java.util.Arrays ;
import java.util.Random ;

public class RobotCity{
  
  public static int[][] buildCity(){
  //we will be using random more than twice
  //1st random group is to create dimensions of the array
  int rows = (int)(Math.random() *5 + 10) ;  
  int columns = (int)(Math.random() *5 + 10) ;  
  //declare and initalize the array
  int[][] cityArray = new int[rows][columns] ;
  for(int a=0;a<cityArray.length;a++){
    for(int b=0;b<cityArray[a].length;b++){
      //2nd group of random usage
      cityArray[a][b] =  (int)(Math.random() *899 + 100) ;  
    }
    
  }
  return cityArray;
  }
  
  public static void display(int[][] array){
   
    System.out.print("{");
    for(int a =0;a<array.length;a++){
      System.out.println("");
       for(int b =0;b<array[a].length;b++){
         System.out.printf(" " + array[a][b] );
         
       }
    }
    System.out.print("}");
  }
  
  public static int[][] invade(int [][] array, int k){
    //for each robot
    for(int i =0; i<k; i++){
       int row = (int)(Math.random() * array.length) ;  
       int column= (int)(Math.random() * array[0].length) ; 
       if ( array[row][column] > 0){
          //set it to the same negatuve value  
          array[row][column] = -( array[row][column]) ;
       }
       else{
         //else it doesnt count
         i-- ;
       } 
    }
    return array ;
  }
  
  public static int[][] update(int[][] array){
   //we have to work backwards in case that there are two  negative values next to each other
    for(int a = (array.length-1); a >= 0; a--){
      for(int b = (array[a].length-1); b >= 0; b--){
        if(array[a][b] < -100 && b != (array[a].length-1)) {
          array[a][b] = -1 *  array[a][b] ;
          array[a][b+1]  = -1 *  array[a][b+1] ;
        }
        if(array[a][b] < -100 && b == (array[a].length-1)){
           array[a][b] = -1 *  array[a][b] ;
        }
      }
    } return array;
  }
  
  public static void main(String[] args){
    int k = 0;
    for(int i=0; i<5 ; i++){
      //random number of robots
      k =  (int)(Math.random() *6 + 5) ; 
      //cityArray creation
      int[][] cityArray = buildCity() ;
      display(cityArray);
      System.out.println("");
      cityArray = invade(cityArray,  k);
      System.out.println("");
      display(cityArray) ;
      cityArray = update(cityArray);
      System.out.println("");
      display(cityArray);
      System.out.println("");
      System.out.println("");
      System.out.println("");
      System.out.println("");
    }
    
  }
}
