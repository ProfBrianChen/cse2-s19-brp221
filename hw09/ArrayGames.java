//Bratislav Petkovic
//4-12-2019
//hw09

//The method insert() will accept two arrays as inputs.  It will produce a new array long enough
//to contain both arrays, and both input arrays should be inserted into the new array in the following 
//manner: select a random member of the first array, and insert every member of the second array in before
//the first array.  Thus, output should look like this (with smaller arrays):

//Input 1: {1,2,3,4,5,6}	Input 2: {10,11,12,13}	
//Output: {1,2,3,10,11,12,13,4,5,6}

//The method shorten() should accept an input array and an input integer.  It should check if the input
//integer actually refers to an index within the range of the input array’s length.  If not, then return 
//the original array.  Otherwise, return a new array having length that is one member less than the input. 
//The output array in that case should have the member referenced by the input index removed.  E.g:

//Input1: {1,2,3,4,5,6,7}	Input 2: 25
//Output: {1,2,3,4,5,6,7}

//Input1: {1,2,3,4,5,6,7}	Input 2: 5
//Output: {1,2,3,4,5,7}


import java.util.Scanner ;
import java.util.Arrays ;
import java.util.Random ;
import java.util.ArrayList;

public class ArrayGames{
  
  public static int [] generate(){
    //create an array with size of 10-20 characters
    int arraySize = (int)(Math.random() * 10) + 10  ;
    int[] array = new int[arraySize] ; 
    for (int a = 0 ; a < arraySize ; a ++){
      array[a] = (int)(Math.random() * 20) ; 
    }
  return array ; 
  }
  
  public static void print(int [] array){
    //method that will print out the array given to it
    System.out.print("[") ;
    
    for (int a = 0 ; a < array.length ; a ++){
      System.out.print(array[a] + " ") ;
    }
    System.out.println("]") ;
  }
  
  public static int[] insert(int[] array1, int[] array2){           //array1 = [0, 1, 2, 3, 4, 5, 6, ]         //aray2 = [21, 22, 24, 25]
    int[] newArray = new int[array1.length + array2.length] ;       // 7 + 4 = 11
    int randomlySelectedIndex = (int)(Math.random() * array1.length-2 ) + 1 ;   // 3  
    
    for(int i = 0; i < randomlySelectedIndex; i ++){                //newArray = [0, 1, 2]
      newArray[i] = array1[i] ; 
    }
    
    int c = 0 ;
    for(int a = randomlySelectedIndex; a < (randomlySelectedIndex + array2.length); a ++ ){ 
        newArray[a] = array2[c] ;                 //newArray =  [0, 1, 2, 21, 22, 24, 25, ]
        c++ ;  
    }
    
    for(int b =0; b<((array1.length + array2.length) - (randomlySelectedIndex + array2.length)); b++ ){                      
      newArray[(randomlySelectedIndex + array2.length) + b] = array1[randomlySelectedIndex + b] ;
    }
    
    
    return newArray;
  }
  
  public static int[] shorten(int[] array, int index ){
    
    if((array.length-1) < index){
      return array ;}
    else{
      int[] returningArray = new int[array.length - 1] ;         //array = [0,1,2,3,4,5,6,24,54] //index to be popped= 4
      for (int i = 0 ; i < (index) ; i++){
        returningArray[i] = array[i];
      }
      for (int e = index ; e < (array.length - 1) ; e++){
        returningArray[e] = array[e + 1];
          
        }
      return returningArray;
      }
    
    }
    
  
  
  
  
  public static void main(String[] args){
    
    Scanner userInput = new Scanner(System.in) ;
    
    boolean check = false ;
    while(check == false){
      System.out.println("Would you like to use [shorten] or [insert] method?") ;
      String methodChoice = userInput.next() ;
      if( methodChoice.equals("shorten")){
        int [] generatedArray1 = generate();
        System.out.println("Index which you want to remove?") ;
        int intIndex = userInput.nextInt() ;
        System.out.println("Input 1: ");
        print(generatedArray1); 
        System.out.println("Input 2: " + intIndex);
        System.out.println("Output 1: "); 
        print(shorten(generatedArray1, intIndex ));
        check = true ; 
      }
      else if( methodChoice.equals("insert")){
        int [] generatedArray1 = generate();
    		int [] generatedArray2 = generate();
        System.out.println("Input 1: "); 
        print(generatedArray1);
        System.out.println("Input 2 :");
        print(generatedArray2);
        System.out.println("Output 1: ");
        print(insert(generatedArray1, generatedArray2 ));
        check = true ; 
      }
      else if( methodChoice.equals("Shorten")){
        int [] generatedArray1 = generate();
        System.out.println("Index which you want to remove?") ;
        int intIndex = userInput.nextInt() ;
        System.out.println("Input 1: ");
        print(generatedArray1); 
        System.out.println("Input 2: " + intIndex);
        System.out.println("Output 1: "); 
        print(shorten(generatedArray1, intIndex ));
        check = true ; 
      }
      else if( methodChoice.equals("Insert")){
        int [] generatedArray1 = generate();
    		int [] generatedArray2 = generate();
        System.out.println("Input 1: "); 
        print(generatedArray1);
        System.out.println("Input 2 :");
        print(generatedArray2);
        System.out.println("Output 1: ");
        print(insert(generatedArray1, generatedArray2 ));
        check = true ; 
      }
    }
    
    
  }
}