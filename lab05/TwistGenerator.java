
//lab06
//03-04-19
//The program uses a while loop to outprint a series of characters

import java.util.Scanner ;

public class TwistGenerator{
  public static void main(String[] args){
    
    //initalize variables
    String beginning = "\\ /" ;
    String middle = " X " ;
    String end = "/ \\" ;
    int mainCount = 0;
    int count = 0;
    int count1 = 0;
    int count2 = 0;
    //introduce an input 
    Scanner myInput = new Scanner(System.in) ;
    //communicate to the user
    System.out.print("Please enter a positive integer : ");
    //initialize length the variable
    int length = myInput.nextInt() ; 
    
    while (mainCount <= (length/3)){
    //while loop for the top portion
      while (count <= (length/3)) {
        if (length % 3 == 0){
        System.out.print(beginning);
        }
        else if (length % 3 == 1){
        System.out.print(beginning);
          if (count==(length/3)){
            System.out.print("\\");
          }}
        else if (length % 3 == 2){
        System.out.print(beginning);
          if (count==(length/3)){
            System.out.print("\\ ");
          }
        }
      // interate count
       count += 1 ;                  
    }
       //while loop for the middle portion
       System.out.println("");
       while (count1 <= (length/3)) {
        if (length % 3 == 0){
        System.out.print(middle);
        }
        else if (length%3 == 1){
        System.out.print(middle);
          if (count1==(length/3)){
            System.out.print(" ");
          }}
        else if (length%3 == 2){
        System.out.print(middle);
          if (count1==(length/3)){
            System.out.print(" X");
          }
        }
      // interate count
       count1 += 1 ;                  
    }
       //while loop for the bottom portion
       System.out.println("");
       while (count2 <= (length/3)) {
        if (length%3 == 0){
        System.out.print(end);
        }
        else if (length%3 == 1){
        System.out.print(end);
          if (count2==(length/3)){
            System.out.print("/");
          }}
        else if (length%3 == 2){
        System.out.print(end);
          if (count2==(length/3)){
            System.out.print("/ ");
          }
        }
      // interate count
       count2 += 1 ;                  
    }
     mainCount += 1 ;
    }
  System.out.println("");
  }
    
  }
  
