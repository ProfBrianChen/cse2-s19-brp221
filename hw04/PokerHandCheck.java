//Bratislav Petkovic Homework 4 
//2-18-2019

// this program mimics a poker game. 
//Suppose you randomly drew five cards, each from a different shuffled deck. 
//Implement a program that detects whether the five cards contain a pair,
//two pair, or three of a kind. If none of these special hands exist, report
//that you have a “high card hand”.

public class PokerHandCheck{
  public static void main(String[] args){
      int rand_number;
      int card_number;
      int true_card_number;
      float suit;
      int count;    
      int counter_value_1 = 0;
      int counter_value_2 = 0;
      int counter_value_3 = 0;
      int counter_value_4 = 0;
      int counter_value_5 = 0;
      int counter_value_6 = 0;
      int counter_value_7 = 0;
      int counter_value_8 = 0;
      int counter_value_9 = 0;
      int counter_value_10 = 0;
      int counter_value_11 = 0;
      int counter_value_12 = 0;
      int counter_value_13 = 0;
    
      //1st card
      rand_number = (int)(Math.random()*52)+1;
      true_card_number = (rand_number+13) % 13 ;
      suit = rand_number / 13 ;
      if (suit == 0) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of diamonds ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of diamonds ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of diamonds ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of diamonds ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of diamonds ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of diamonds ");
        }}
      else if (suit == 1) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of clubs ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of clubs ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of clubs ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of clubs ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of clubs ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of clubs ");
        }}
      else if (suit == 2) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of hearts ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of hearts ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of hearts ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of hearts ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of hearts ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of hearts ");
        }}
      else if (suit == 3 || suit == 4) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of spades ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of spades ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of spades ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of spades ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of spades ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of spades ");
        }}
      if (true_card_number == 1){
        counter_value_1 ++;
      }
      if (true_card_number == 2){
        counter_value_2 ++;
      }
      if (true_card_number == 3){
        counter_value_3 ++;
      }
      if (true_card_number == 4){
        counter_value_4 ++;
      }
      if (true_card_number == 5){
        counter_value_5 ++;
      }
      if (true_card_number == 6){
        counter_value_6 ++;
      }
      if (true_card_number == 7){
        counter_value_7 ++;
      }
      if (true_card_number == 8){
        counter_value_8 ++;
      }
      if (true_card_number == 9){
        counter_value_9 ++;
      }
      if (true_card_number == 10){
         counter_value_10 ++;
      }
      if (true_card_number == 11){
        counter_value_11 ++;
      }
      if (true_card_number == 12){
        counter_value_12 ++;
      } 
      if (true_card_number == 0){
        counter_value_13 ++;
      }
      
      //2nd card
      rand_number = (int)(Math.random()*52)+1;
      true_card_number = (rand_number+13) % 13 ;
      suit = rand_number / 13 ;
      if (suit == 0) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of diamonds ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of diamonds ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of diamonds ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of diamonds ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of diamonds ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of diamonds ");
        }}
      else if (suit == 1) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of clubs ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of clubs ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of clubs ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of clubs ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of clubs ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of clubs ");
        }}
      else if (suit == 2) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of hearts ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of hearts ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of hearts ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of hearts ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of hearts ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of hearts ");
        }}
      else if (suit == 3 || suit == 4) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of spades ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of spades ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of spades ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of spades ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of spades ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of spades ");
        }}
      if (true_card_number == 1){
        counter_value_1 ++;
      }
      if (true_card_number == 2){
        counter_value_2 ++;
      }
      if (true_card_number == 3){
        counter_value_3 ++;
      }
      if (true_card_number == 4){
        counter_value_4 ++;
      }
      if (true_card_number == 5){
        counter_value_5 ++;
      }
      if (true_card_number == 6){
        counter_value_6 ++;
      }
      if (true_card_number == 7){
        counter_value_7 ++;
      }
      if (true_card_number == 8){
        counter_value_8 ++;
      }
      if (true_card_number == 9){
        counter_value_9 ++;
      }
      if (true_card_number == 10){
         counter_value_10 ++;
      }
      if (true_card_number == 11){
        counter_value_11 ++;
      }
      if (true_card_number == 12){
        counter_value_12 ++;
      } 
      if (true_card_number == 0){
        counter_value_13 ++;
      }
    
      //3rd card
      rand_number = (int)(Math.random()*52)+1;
      true_card_number = (rand_number+13) % 13 ;
      suit = rand_number / 13 ;
      if (suit == 0) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of diamonds ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of diamonds ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of diamonds ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of diamonds ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of diamonds ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of diamonds ");
        }}
      else if (suit == 1) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of clubs ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of clubs ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of clubs ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of clubs ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of clubs ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of clubs ");
        }}
      else if (suit == 2) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of hearts ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of hearts ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of hearts ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of hearts ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of hearts ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of hearts ");
        }}
      else if (suit == 3 || suit == 4) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of spades ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of spades ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of spades ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of spades ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of spades ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of spades ");
        }}
      if (true_card_number == 1){
        counter_value_1 ++;
      }
      if (true_card_number == 2){
        counter_value_2 ++;
      }
      if (true_card_number == 3){
        counter_value_3 ++;
      }
      if (true_card_number == 4){
        counter_value_4 ++;
      }
      if (true_card_number == 5){
        counter_value_5 ++;
      }
      if (true_card_number == 6){
        counter_value_6 ++;
      }
      if (true_card_number == 7){
        counter_value_7 ++;
      }
      if (true_card_number == 8){
        counter_value_8 ++;
      }
      if (true_card_number == 9){
        counter_value_9 ++;
      }
      if (true_card_number == 10){
         counter_value_10 ++;
      }
      if (true_card_number == 11){
        counter_value_11 ++;
      }
      if (true_card_number == 12){
        counter_value_12 ++;
      } 
      if (true_card_number == 0){
        counter_value_13 ++;
      }
      
      //4th card
      rand_number = (int)(Math.random()*52)+1;
      true_card_number = (rand_number+13) % 13 ;
      suit = rand_number / 13 ;
      if (suit == 0) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of diamonds ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of diamonds ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of diamonds ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of diamonds ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of diamonds ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of diamonds ");
        }}
      else if (suit == 1) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of clubs ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of clubs ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of clubs ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of clubs ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of clubs ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of clubs ");
        }}
      else if (suit == 2) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of hearts ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of hearts ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of hearts ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of hearts ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of hearts ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of hearts ");
        }}
      else if (suit == 3 || suit == 4) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of spades ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of spades ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of spades ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of spades ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of spades ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of spades ");
        }}
      if (true_card_number == 1){
        counter_value_1 ++;
      }
      if (true_card_number == 2){
        counter_value_2 ++;
      }
      if (true_card_number == 3){
        counter_value_3 ++;
      }
      if (true_card_number == 4){
        counter_value_4 ++;
      }
      if (true_card_number == 5){
        counter_value_5 ++;
      }
      if (true_card_number == 6){
        counter_value_6 ++;
      }
      if (true_card_number == 7){
        counter_value_7 ++;
      }
      if (true_card_number == 8){
        counter_value_8 ++;
      }
      if (true_card_number == 9){
        counter_value_9 ++;
      }
      if (true_card_number == 10){
         counter_value_10 ++;
      }
      if (true_card_number == 11){
        counter_value_11 ++;
      }
      if (true_card_number == 12){
        counter_value_12 ++;
      } 
      if (true_card_number == 0){
        counter_value_13 ++;
      }
    
      //5th card
      rand_number = (int)(Math.random()*52)+1;
      true_card_number = (rand_number+13) % 13 ;
      suit = rand_number / 13 ;
      if (suit == 0) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of diamonds ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of diamonds ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of diamonds ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of diamonds ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of diamonds ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of diamonds ");
        }}
      else if (suit == 1) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of clubs ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of clubs ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of clubs ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of clubs ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of clubs ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of clubs ");
        }}
      else if (suit == 2) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of hearts ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of hearts ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of hearts ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of hearts ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of hearts ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of hearts ");
        }}
      else if (suit == 3 || suit == 4) {
        if (true_card_number == 1) {
         System.out.println("You picked an ace of spades ");
        }
        else if (true_card_number == 11) {
         System.out.println("You picked a Jack of spades ");
        }
        else if (true_card_number == 12) {
         System.out.println("You picked a Queen of spades ");
        }
        else if (true_card_number == 0) {
         System.out.println("You picked a King of spades ");
        }
        else if (true_card_number == 8) {
         System.out.println("You picked an eight of spades ");
        }
        else {
         System.out.println("You picked a " + true_card_number  + " of spades ");
        }}
      if (true_card_number == 1){
        counter_value_1 ++;
      }
      if (true_card_number == 2){
        counter_value_2 ++;
      }
      if (true_card_number == 3){
        counter_value_3 ++;
      }
      if (true_card_number == 4){
        counter_value_4 ++;
      }
      if (true_card_number == 5){
        counter_value_5 ++;
      }
      if (true_card_number == 6){
        counter_value_6 ++;
      }
      if (true_card_number == 7){
        counter_value_7 ++;
      }
      if (true_card_number == 8){
        counter_value_8 ++;
      }
      if (true_card_number == 9){
        counter_value_9 ++;
      }
      if (true_card_number == 10){
         counter_value_10 ++;
      }
      if (true_card_number == 11){
        counter_value_11 ++;
      }
      if (true_card_number == 12){
        counter_value_12 ++;
      } 
      if (true_card_number == 0){
        counter_value_13 ++;
      }

      //two pair means that out of the 5 cards, 2 cards have the same value and another two cards have the same value
      if (counter_value_1 == 2 && (counter_value_1 == 2 || counter_value_2 == 2 || counter_value_3 == 2 || counter_value_4 == 2 || counter_value_5 == 2 || counter_value_6 == 2 || counter_value_7 == 2 || counter_value_8 == 2 || counter_value_9 == 2 || counter_value_10 == 2 || counter_value_11 == 2 || counter_value_12 == 2 || counter_value_13 == 2)){
        System.out.print("You have two pairs");
      }
       else if (counter_value_2 == 2 && (counter_value_3 == 2 || counter_value_4 == 2 || counter_value_5 == 2 || counter_value_6 == 2 || counter_value_7 == 2 || counter_value_8 == 2 || counter_value_9 == 2 || counter_value_10 == 2 || counter_value_11 == 2 || counter_value_12 == 2 || counter_value_13 == 2)){
        System.out.print("You have two pairs");
      }
       else if (counter_value_3 == 2 && (counter_value_4 == 2 || counter_value_5 == 2 || counter_value_6 == 2 || counter_value_7 == 2 || counter_value_8 == 2 || counter_value_9 == 2 || counter_value_10 == 2 || counter_value_11 == 2 || counter_value_12 == 2 || counter_value_13 == 2)){
        System.out.print("You have two pairs");
      }
       else if (counter_value_4 == 2 && (counter_value_5 == 2 || counter_value_6 == 2 || counter_value_7 == 2 || counter_value_8 == 2 || counter_value_9 == 2 || counter_value_10 == 2 || counter_value_11 == 2 || counter_value_12 == 2 || counter_value_13 == 2)){
        System.out.print("You have two pairs");
      }
       else if (counter_value_5 == 2 && (counter_value_6 == 2 || counter_value_7 == 2 || counter_value_8 == 2 || counter_value_9 == 2 || counter_value_10 == 2 || counter_value_11 == 2 || counter_value_12 == 2 || counter_value_13 == 2)){
        System.out.print("You have two pairs");
      }
       else if (counter_value_6 == 2 && (counter_value_7 == 2 || counter_value_8 == 2 || counter_value_9 == 2 || counter_value_10 == 2 || counter_value_11 == 2 || counter_value_12 == 2 || counter_value_13 == 2)){
        System.out.print("You have two pairs");
      }
       else if (counter_value_7 == 2 && (counter_value_8 == 2 || counter_value_9 == 2 || counter_value_10 == 2 || counter_value_11 == 2 || counter_value_12 == 2 || counter_value_13 == 2)){
        System.out.print("You have two pairs");
      }
       else if (counter_value_8 == 2 && (counter_value_9 == 2 || counter_value_10 == 2 || counter_value_11 == 2 || counter_value_12 == 2 || counter_value_13 == 2)){
        System.out.print("You have two pairs");
      }
       else if (counter_value_9 == 2 && (counter_value_10 == 2 || counter_value_11 == 2 || counter_value_12 == 2 || counter_value_13 == 2)){
        System.out.print("You have two pairss");
      }
       else if (counter_value_10 == 2 && (counter_value_11 == 2 || counter_value_12 == 2 || counter_value_13 == 2)){
        System.out.print("You have two pairs");
      }
       else if (counter_value_11 == 2 && (counter_value_12 == 2 || counter_value_13 == 2)){
        System.out.print("You have two pairs");
      }
       else if (counter_value_12 == 2 && (counter_value_13 == 2)){
        System.out.print("You have two pairs");
      }
      //three pair means that out of the 5 cards, 3 cards have the same value 
       else if (counter_value_1 == 3){
        System.out.print("You have 3 of a kind : aces");
      }
       else if (counter_value_2 == 3){
        System.out.print("You have 3 of a kind : 2s");
      }
       else if (counter_value_3 == 3){
        System.out.print("You have 3 of a kind : 3s");
      }
       else if (counter_value_4 == 3){
        System.out.print("You have 3 of a kind : 4s");
      }
       else if (counter_value_5 == 3){
        System.out.print("You have 3 of a kind : 5s");
      }
       else if (counter_value_6 == 3){
        System.out.print("You have 3 of a kind : 6s");
      }
       else if (counter_value_7 == 3){
        System.out.print("You have 3 of a kind : 7s");
      }
       else if (counter_value_8 == 3){
        System.out.print("You have 3 of a kind : 8s");
      }
       else if (counter_value_9 == 3){
        System.out.print("You have 3 of a kind : 9s");
      }
       else if (counter_value_10 == 3){
        System.out.print("You have 3 of a kind :10s");
      }
       else if (counter_value_11 == 3){
        System.out.print("You have 3 of a kind : Jacks");
      }
       else if (counter_value_12 == 3){
        System.out.print("You have 3 of a kind : Queens");
      }
       else if (counter_value_13 == 3){
        System.out.println("You have 3 of a kind : Kings");
      }
     //high card hand means that there are no pairs at all. 
       else if( counter_value_1 <= 1 && counter_value_2 <= 1 && counter_value_3 <= 1 && counter_value_4 <= 1 && counter_value_5 <= 1 && counter_value_6 <= 1 && counter_value_7 <= 1 && counter_value_8 <= 1 && counter_value_9 <= 1 && counter_value_10 <= 1 && counter_value_11 <= 1 && counter_value_12 <= 1 && counter_value_13 <= 1){
        System.out.println("You have a high card hand");
      }
        else {
         if (counter_value_1 == 2){
        System.out.print("You have a pair of aces");
      }
       else if (counter_value_2 == 2){
        System.out.print("You have a pair of 2s");
      }
       else if (counter_value_3 == 2){
        System.out.print("You have a pair of 3s");
      }
       else if (counter_value_4 == 2){
        System.out.print("You have a pair of 4s");
      }
       else if (counter_value_5 == 2){
        System.out.print("You have a pair of 5s");
      }
       else if (counter_value_6 == 2){
        System.out.print("You have a pair of 6s");
      }
       else if (counter_value_7 == 2){
        System.out.print("You have a pair of 7s");
      }
       else if (counter_value_8 == 2){
        System.out.print("You have a pair of 8s");
      }
       else if (counter_value_9 == 2){
        System.out.print("You have a pair of 9s");
      }
       else if (counter_value_10 == 2){
        System.out.print("You have a pair of 10s");
      }
       else if (counter_value_11 == 2){
        System.out.print("You have a pair of Jacks");
      }
       else if (counter_value_12 == 2){
        System.out.print("You have a pair of Queens");
      }
       else if (counter_value_13 == 2){
        System.out.println("You have a pair of Kings");
      }
       }
  System.out.println("");
  }
    }
    
  
          