//Bratislav Petkovic 
//March 22nd, 2019
//lab07
//Objective: Generating sentences by using 4 different methods

import java.util.Random;
import java.util.Scanner;

public class lab07scratch {
    
  public static String adjectives(){
    Random randomGenerator = new Random() ;       //making the Random class instance 
    int randomInt = randomGenerator.nextInt(10);  //this statement generates an integer between 0 and 10
    String adjective = "" ;
    switch(randomInt){
      case 0:
        adjective = "vengeful" ;
        break;
      case 1:
        adjective = "rebellious" ;
        break;
      case 2:
        adjective = "cowardly" ;
        break;
      case 3:
        adjective = "foolish" ;
        break;
      case 4:
        adjective = "sly" ;
        break;
      case 5:
        adjective = "dexterious" ;
        break;
      case 6:
        adjective = "lustful" ;
        break;
      case 7:
        adjective = "greedy" ;
        break;
      case 8:
        adjective = "loyal" ;
        break;
      case 9:
        adjective = "agitated" ;
        break;
      
      
    
    }
     return adjective ;
  }
  public static String nounsSubject(){
    Random randomGenerator = new Random() ;       //making the Random class instance 
    int randomInt = randomGenerator.nextInt(10);  //this statement generates an integer between 0 and 10
    String subject = "" ;
    switch(randomInt){
        case 0:
          subject = "Silly Rabbit" ;
          break;
        case 1:
          subject = "Jack" ;
          break;
        case 2:
          subject = "Juggler" ;
          break;
        case 3:
          subject = "Joker" ;
          break;
        case 4:
          subject = "Corporate Monkey" ;
          break;
        case 5:
          subject = "Computer Geek" ;
          break;
        case 6:
          subject = "Frat guy" ;
          break;
        case 7:
          subject = "professor" ;
          break;
        case 8:
          subject = "janitor" ;
          break;
        case 9:
          subject = "doctor" ;
          break;
        
        
       
    }
     return subject;
  }
  public static String pastTenseVerbs(){
    Random randomGenerator = new Random() ;       //making the Random class instance 
    int randomInt = randomGenerator.nextInt(10);  //this statement generates an integer between 0 and 10
    String pastTenseVerb = "" ;
   
    switch(randomInt){
        case 0:
          pastTenseVerb = "murdered";
          break;
        case 1:
          pastTenseVerb = "fought";
          break;
        case 2:
          pastTenseVerb = "punished";
          break;
        case 3:
          pastTenseVerb = "entertained";
          break;
        case 4:
          pastTenseVerb = "wrote to ";
          break;
        case 5:
          pastTenseVerb = "deceived";
          break;
        case 6:
          pastTenseVerb = "tricked";
          break;
        case 7:
          pastTenseVerb = "married";
          break;
        case 8:
          pastTenseVerb = "kissed";
          break;
        case 9:
          pastTenseVerb = "hugged";
          break;
        
        
       
    }
     return pastTenseVerb;
  }
  public static String nounsObject(){
    Random randomGenerator = new Random() ;       //making the Random class instance 
    int randomInt = randomGenerator.nextInt(10);  //this statement generates an integer between 0 and 10
    String object = "" ;

    switch(randomInt){
        case 0:
          object = "Beyonce" ;
          break ;
        case 1:
          object = "student" ;
          break ;
        case 2:
          object = "Teacher Assistants" ;
          break ;
        case 3:
          object = "Noah the Boa" ;
          break ;
        case 4:
          object = "King" ;
          break ;
        case 5:
          object = "dragon" ;
          break ;
        case 6:
          object = "Queen" ;
          break ;
        case 7:
          object = "knight" ;
          break ;
        case 8:
          object = "artist" ;
          break ;
        case 9:
          object = "pirate" ;
          break ;
        
        
        
    }
   return object;
  }
  
  public static String thesisSentence(String adj, String sub, String verb, String obj){
      String thesisResult = "The " +  adj + " " + sub + " " + verb + " the " + obj + "." ;
      return thesisResult ; 
  }

  public static String supportingSentences(String adj, String sub, String verb, String obj){
      
      Random randomGenerator = new Random() ;       //making the new Random class instance inside of the scope of this method 
      int randomInt = randomGenerator.nextInt(2);  //this statement generates an integer between 0 and 9
      String supportingSentence = "" ; 
      
      switch(randomInt){
        case 0:
           supportingSentence = "It then " + verb + " the " + adj + " " + obj + "." ;
          break;
        case 1:
           supportingSentence = "The " +  sub + " " + verb + " the "+ adj + " " + obj + "." ;
          break; 
      }    
      return supportingSentence ;
  }
  
  public static String cocnlusionSentence(String sub, String verb, String obj){
      String conclusionSentence1 = "Finally, the " + obj + " " + verb + " the " + sub  ;
      
      return conclusionSentence1 ;
  }
      
  
  //main method where everything goes down
  public static void main(String[] args){

      Scanner userInput = new Scanner(System.in) ;  //making the Scanner class instance
      Random randomGenerator = new Random() ;       //making the new Random class instance inside of the scope of this method 
      
      String adjectiveWord = adjectives() ; 
      String subjectWord = nounsSubject() ;
      String verbWord = pastTenseVerbs() ;
      String objectWord = nounsObject() ;
      String input = "" ;
      String supportingSentence = "" ;
      int i = 0;
      
      System.out.println("") ;
     
      //phase 1
      String thesisInstance = thesisSentence( adjectiveWord,  subjectWord,  verbWord,  objectWord);
      System.out.println(thesisInstance) ;
      
      //phase 2
      while (i<1) { //as long as u keep saying yes the loop will run forever. 
        int randomInt2 = randomGenerator.nextInt(6); 
        System.out.println("Would you like mores sentences ?  [yes] or [no] ");
        input = userInput.next() ;

        //.equals example: boolean isEqual = str1.equals(str2);
        
        if (input.equals("yes") || input.equals("YES")){
          for(int count = 0; count < randomInt2 ; count++){  
            String adjectiveWord2 = adjectives() ; 
            String verbWord2 = pastTenseVerbs() ;
            String objectWord2 = nounsObject() ;

            String supportingSentenceInstance = supportingSentences( adjectiveWord2,  subjectWord,  verbWord2,  objectWord2 ) ;
            System.out.println(supportingSentenceInstance) ;

          }}
        else {
          System.out.println(" ") ;  
          System.out.println("Your loss. ") ;  
             break ;            //break out of the loop 
        }
        
        i++ ;
      
      }
      
      //conclusiion sentence phase.
      String verbWord3 = pastTenseVerbs() ;
      String objectWord3 = nounsObject() ;
      String lastSentence = cocnlusionSentence(subjectWord, verbWord3, objectWord3 ) ;
      System.out.println(lastSentence) ;
      
      System.out.println("") ;
      System.out.println("         THE  END") ;
  
  }}