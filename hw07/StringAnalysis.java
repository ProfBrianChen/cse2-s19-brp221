//Bratislav Petkovic
//3-23-2019
//hw07 partB: analyzing if the string input has letters 

import java.util.Scanner ;
import java.util.Random ;
import java.lang.Character ;

public class StringAnalysis{
  
  public static boolean limitedCharacters(String stringInput, int input){
    
    boolean stringOrNah = true ;
    for(int i=0; i<=input ; i++){
      char specificChar = stringInput.charAt(i) ;
      boolean boolVar = Character.isLetter(specificChar) ;
      
      if (boolVar == false ){
        stringOrNah = false ;
        break ;
      }
      else if (boolVar == true ){
        stringOrNah = true ;
        
      }
       
  }
  return stringOrNah ;
  }
  
  public static boolean allCharacters(String stringInput){
      int stringLength = stringInput.length() ;
      boolean stringOrNah = true ;
      for(int i =0; i<stringLength; i++){
        char specificChar = stringInput.charAt(i) ;
        boolean boolVar = Character.isLetter(specificChar) ;
        
        if (boolVar == false ){
          stringOrNah = false ;
          break ;
          }
        else if (boolVar == true ){
          stringOrNah = true ;
        
      }
       
  }
  return stringOrNah ;
  }
  
  public static void main(String[] args){
    
    Scanner userInput = new Scanner(System.in) ;
    

    System.out.println("Type in a string");
    
    String usersString = userInput.next() ;
    
    int stringLength = usersString.length() ;

    System.out.println("How many characters do you want to analyze");
    int userStringLength = userInput.nextInt() ;

    if (stringLength == userStringLength){
      System.out.println(allCharacters(usersString)) ;
      
    }
    else if (stringLength < userStringLength){
      userStringLength = userStringLength - (userStringLength-stringLength) ; 
      System.out.println(limitedCharacters(usersString, userStringLength )) ;
    }
    else if (stringLength > userStringLength) {
      System.out.println(limitedCharacters(usersString, userStringLength )) ; 
    }
  }
  
}

