//hw06
//Bratislav Petkovic
//3/18/19
import java.util.Scanner;

public class Network {
    public static void main (String[] args) {
        Scanner userInput = new Scanner( System.in );
        String garbage = "";

        int height = 0; 													                  //variable initialization
        System.out.println("Enter a positive integer for height.");
        boolean checkInputHeight = userInput.hasNextInt();					//checking if user entered correct input for height
        if (checkInputHeight == true) {
            height = userInput.nextInt();
            if (height < 1 ) {
                checkInputHeight = false; 										// is input a positive integer between 1 and 10
            }
        }
        while (checkInputHeight == false) {
            if (height == 0) {
                garbage = userInput.next(); 									//get rid of incorrect inputs
            }
            System.out.println("Input a positive integer please!");			// tell user what they are doing incorrectly
            checkInputHeight = userInput.hasNextInt();
            if (checkInputHeight) {											// if TRUE
                height = userInput.nextInt(); 									//assign a new value which is the true value
                if (height < 1) {
                    checkInputHeight = false; 									// is input a positive integer between 1 and 10
                }
                else {
                    break; 														//break the loop if the number is invalid
                }
            }
        }

        int width = 0; 														//initialization of width variable
        System.out.println("Enter a positive integer for width.");
        boolean checkInputWidth = userInput.hasNextInt();					//checking if user entered correct input for width
        if (checkInputWidth == true) {
            width = userInput.nextInt();
            if (width < 1 ) {
                checkInputWidth = false; 										//is input a positive integer between 1 and 10
            }
        }
        while (checkInputWidth == false) {
            if (width == 0) {
                garbage = userInput.next(); 									//get rid of incorrect inputs
            }
            System.out.println("Input a positive integer please!");
            checkInputWidth = userInput.hasNextInt();
            if (checkInputWidth == true) {
                width = userInput.nextInt(); 									//assign a new value
                if (width < 1 ) {
                    checkInputWidth = false; 										//is input a positive integer between 1 and 10
                }
                else {
                    break; 														//break the loop if the number is invalid
                }
            }
        }

        int squareSize = 0; 												//initialization of variable
        System.out.println("Enter a positive integer for square size.");
        boolean squareCheck = userInput.hasNextInt();
        if (squareCheck == true) {
            squareSize = userInput.nextInt();
            if (squareSize < 1 ) {							//is input a positive integer between 1 and 10
                squareCheck = false;
            }
        }
        while (squareCheck == false) {
            if (squareSize == 0) {
                garbage = userInput.next(); 									//get rid of incorrect inputs
            }
            System.out.println("You need input a positive integer please!");
            squareCheck = userInput.hasNextInt();
            if (squareCheck == true) {
                squareSize = userInput.nextInt(); 								//assign a new value
                if (squareSize < 1) {							//is input a positive integer between 1 and 10
                    squareCheck = false;
                }
                else {
                    break; 														//break the loop if the number is invalid
                }
            }
        }

        int edgeLength = 0; 												//variable that user will enter
        System.out.println("Enter a positive integer for edge length.");
        boolean edgeCheck = userInput.hasNextInt();
        if (edgeCheck == true) {
            edgeLength = userInput.nextInt();
            if (edgeLength < 1 ) {
                edgeCheck = false; 												//is input a positive integer between 1 and 10
            }
        }
        while (edgeCheck == false) {
            if (edgeLength == 0) {
                garbage = userInput.next(); 									//get rid of incorrect inputs
            }
            System.out.println("You need input a positive integer!");
            edgeCheck = userInput.hasNextInt();
            if (edgeCheck == true) {
                edgeLength = userInput.nextInt(); 								//assign a new value
                if (edgeLength < 1 ) {
                    edgeCheck = false; 											//is input a positive integer between 1 and 10
                }
                else {
                    break; 														//break the loop if the number is invalid
                }
            }
        }

        String top = ""; 													//"#"
        String topEdge = ""; 												//"||"
        String side = ""; 													//"|"
        String sideEdge = ""; 												// "-"
        for(int w = 0; w < width;){ 										//loop for width
            for (int t = 1; t <= squareSize; t++) { 							//top and bottom edges of a single square
                if (t == 1 || squareSize == t) {
                    top = top + "#";
                    side = side + "|";
                    sideEdge = sideEdge + "|";
                    if (squareSize == 1) {  										//special case: if there is only one square
                        topEdge = topEdge + "|";
                        for (int f = 1; f <= edgeLength; f++) {
                            w++;
                            if (w >= width) { 										//leave loop once the desired width is reached
                                break;
                            }
                            top = top + "-";
                            topEdge = topEdge + " ";
                        }
                        if (w >= width) {
                            break;
                        }
                    }
                }
                else {
                    top = top + "-";                         //make top and empty bodies of squares
                    side = side + " ";
                    sideEdge = sideEdge + " ";
                }
                if ((((squareSize % 2) == 1 && t == ((squareSize/2) + (3/2))) || ((squareSize % 2) == 0 && ((t == ((squareSize/2) + (3/2))) || (t == ((squareSize/2) + (1/2)))))) && (squareSize != 1) ) {
                    topEdge = topEdge + "|";
                }
                else if (squareSize == 1){}                  //square w size = 1  is special case
                else {
                    topEdge = topEdge + " ";
                }
                w++;
                if (w >= width) {
                    break;
                }
            }
            if (w >= width) {
                break;
            }
            if ((squareSize != 1) && (squareSize != 2)) {    //square size = 1 || square size 2 squares also special
                for (int b = 1; b <= edgeLength; b++) {
                    sideEdge = sideEdge + "-";
                    side = side + " ";
                    top = top + " ";
                    topEdge = topEdge + " ";
                    w++;
                    if (w >= width) {
                        break;
                    }
                }
            }
            if (squareSize == 2) {                           //make square size = 2 w corners
                for (int b = 1; b <= edgeLength; b++) {
                    top = top + "-";
                    topEdge = topEdge + " ";
                    w++;
                    if (w >= width) {
                        break;
                    }
                }
            }
        }

        for (int a = 0; a < height;){                       //printing lines but keeping the correct number according to the height parameters
            int c = 1;
            int d = 1;
            for (; c <= squareSize; c++) {
                if (c == 1 || squareSize == c){
                    System.out.println(top);               //printing top of the square
                    a++;
                        if (a >= height){                  //leave loop when u reach desired height
                        break;
                    }
                }
                else if ((squareSize % 2) == 1 && c == ((squareSize/2) + (3/2))) { //printing double edges for even squares
                    System.out.println(sideEdge);
                    a++;
                    if (a >= height){
                        break;
                    }
                }
                else if ((squareSize % 2) == 0 && ((c == ((squareSize/2) + (3/2))) || (c==((squareSize/2) + (1/2))))) { //printing single edges for odd squares
                    System.out.println(sideEdge);
                    a++;
                    if (a >= height){
                        break;
                    }
                }
                else {
                    System.out.println(side);             //printing the vertical "|" of the squares
                    a++;
                    if (a >= height){
                        break;
                    }
                }
            }
            if (a>=height){
                break;
            }
            for (; d <= edgeLength; d++) {                 //dont go over the height parameter
                System.out.println(topEdge);                //printing the vertical "|"
                a++;
                if (a == height){
                    break;
                }
            }
            if (a>=height){
                break;
            }
        }
    }
}