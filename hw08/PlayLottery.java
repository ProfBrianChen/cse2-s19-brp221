//Bratislav Petkovic
//hw08
//April 6th,2019
//this program simulates a lottery 

import java.util.Arrays ;
import java.util.Scanner ;

public class PlayLottery{
  public static boolean userWins(int[] user, int[] winning){
  //returns true when user and winning are the same.
    boolean var = true; 
    if (user == winning){
      System.out.print("Good job, you won!! Maybe you should consider gambling");
      var = true;
    }
    else if (user != winning){
      System.out.print("You lose, ha!");
      var = false ;
    }
    return var;
  }

  public static int[] numbersPicked(){
//generates the random numbers for the lottery without 
//duplication.
    int randNum1 = (int)(Math.random()*59)+1;
    int randNum2 = (int)(Math.random()*59)+1;
    int randNum3 = (int)(Math.random()*59)+1;
    int randNum4 = (int)(Math.random()*59)+1;
    int randNum5 = (int)(Math.random()*59)+1;
  
    int winningNumbsArray[] = {randNum1,randNum2,randNum3,randNum4,randNum5};
  
    return winningNumbsArray ;
  }
  
  public static void main(String[] args){
    
    //user input
    Scanner userInput = new Scanner(System.in) ;
    System.out.println("");
    System.out.print("Enter 5 numbers between 0 and 59 : ");
    int num1 = userInput.nextInt() ;
    int num2 = userInput.nextInt() ;
    int num3 = userInput.nextInt() ;
    int num4 = userInput.nextInt() ;
    int num5 = userInput.nextInt() ;
    
    
    int guessedNumbsArray[] = {num1,num2,num3,num4,num5};
     //System.out.print(guessedNumbsArray[0] + " ");
    System.out.println("");
    System.out.print("Your number are : ");
    for (int i= 0 ; i< guessedNumbsArray.length; i++ ){
    System.out.print(guessedNumbsArray[i] + " ");
    }
    
    System.out.println("");
    System.out.print("Winning number are : ");
    for (int i= 0 ; i< numbersPicked().length; i++ ){
    System.out.print(numbersPicked()[i] + " ");
    }
    System.out.println("");
    userWins(guessedNumbsArray, numbersPicked()) ;
    System.out.println("");
    
    
  }
  
  
}