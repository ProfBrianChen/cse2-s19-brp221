//Bratislav P
//02-8-19
//hw03 PROGRAM2

import java.util.Scanner;
public class BoxVolume{
    			// main method required for every Java program
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in ) ;
          
          System.out.print("Enter the height in meters of the box: ");
          double height = myScanner.nextDouble();
          System.out.print("Enter the width in meters of the box: ");
          double width = myScanner.nextDouble();
          System.out.print("Enter the length in meters of the box: ");
          double length = myScanner.nextDouble();
          
          
          double volume;
          volume = height * width * length;
          System.out.println("The volume of the box is " +volume);
           
          
                    


}  //end of main method   
  	} //end of class
