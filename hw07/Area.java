//Bratislav Petkovic
//3-23-2019
//hw07 partA: compute areas of shapes chosen by the user

import java.util.Scanner ;
import java.util.Random ;
import java.lang.Math ;

public class Area{

  public static double rectangleArea( double length, double width){
    double area = length * width ;
    return area ;
  }
  
  public static double triangleArea( double base, double height){
    double area = base * height / 2 ;
    return area ;
  }
  
  public static double circleArea( double radius){
    double area = Math.PI * Math.pow(radius,2) ;
    return area ;
  }
  
  // main method
  public static void main(String[] args){
    
    Scanner userInput = new Scanner(System.in) ;//userInput is the class instance we will be using
    System.out.println("What is the shape of your choice? [triangle] [rectangle] [circle]") ;
    String theShape = userInput.next() ;
    
    if (theShape.equals("rectangle")){
      //use the rectangle area method
      System.out.println("Type in the rectangle length as a double") ;
      double recLength = userInput.nextDouble();
      
      System.out.println("Type in the rectangle width as a double") ;
      double recWidth = userInput.nextDouble();
      
      System.out.println(rectangleArea(recLength,recWidth));
    }
    else if (theShape.equals("triangle")){
      //use the triangle area method
      System.out.println("Type in the triangle base as a double") ;
      double triBase = userInput.nextDouble();
      
      System.out.println("Type in the triangle height as a double") ;
      double triHeight = userInput.nextDouble();
      
      System.out.println(triangleArea(triBase,triHeight));

    }
    else if (theShape.equals("circle")){
      //use the circle area method
      System.out.println("Type in the circle radius as a double") ;
      double circleRadius = userInput.nextDouble();
     
      System.out.println(circleArea(circleRadius));

    }
    else {  
      //in case that the user gives an incorrect shape
      System.out.println("You have not typed in an available shape. Try again !") ;
      while(true){
         System.out.println("What is the shape of your choice? [triangle] [rectangle] [circle]") ;
         theShape = userInput.next() ;
         if (theShape.equals("rectangle")){
        //use the rectangle area method
        System.out.println("Type in the rectangle length as a double") ;
        double recLength = userInput.nextDouble();

        System.out.println("Type in the rectangle width as a double") ;
        double recWidth = userInput.nextDouble();

        System.out.println(rectangleArea(recLength,recWidth));
        break;
      }
      else if (theShape.equals("triangle")){
        //use the triangle area method
        System.out.println("Type in the triangle base as a double") ;
        double triBase = userInput.nextDouble();

        System.out.println("Type in the triangle height as a double") ;
        double triHeight = userInput.nextDouble();

        System.out.println(triangleArea(triBase,triHeight));
        break;
      }
      else if (theShape.equals("circle")){
        //use the circle area method
        System.out.println("Type in the circle radius as a double") ;
        double circleRadius = userInput.nextDouble();

        System.out.println(circleArea(circleRadius));
        break;
    }
      }
    }
  }
  
  
}